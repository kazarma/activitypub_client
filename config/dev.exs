import Config

config :oauth2, debug: true

config :activity_pub_client,
  ecto_repos: [ActivityPubClient.Repo]

config :activity_pub_client, ActivityPubClient.Repo,
  database: "activity_pub_client_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"
