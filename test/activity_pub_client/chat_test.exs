defmodule ActivityPubClient.ChatTest do
  use ExUnit.Case

  import ActivityPubClient.Chat
  import Mock

  describe "get_chat/2" do
    test "sends the corresponding request" do
      with_mock OAuth2.Client,
        post: fn :client, "/api/v1/pleroma/chats/by-account-id/bob@ap_instance" ->
          {:ok, %OAuth2.Response{body: :chat}}
        end do
        assert {:ok, :chat} = get_chat(:client, "bob@ap_instance")
      end
    end
  end

  describe "post_message/4" do
    test "sends the corresponding request" do
      with_mock OAuth2.Client,
        post: fn :client,
                 "/api/v1/pleroma/chats/chat_id/messages",
                 %{content: "Hello!"},
                 [{"Content-Type", "application/json"}] ->
          {:ok, %OAuth2.Response{body: :message}}
        end do
        assert {:ok, :message} = post_message(:client, :chat_id, "Hello!")
      end
    end
  end
end
