defmodule ActivityPubClient.ClientTest do
  use ExUnit.Case
  use ActivityPubClient.DataCase

  import ActivityPubClient.Client

  alias ActivityPubClient.App
  alias ActivityPubClient.User

  @site "http://pleroma.local"
  @ap_id "http://pleroma.local/users/alice"

  setup_all do
    :ok = :meck.new(:hackney)

    # on_exit(fn ->
    #    :meck.unload(:hackney)
    #  end)
  end

  describe "client/2" do
    test "gets the client keys and creates an App record on first call" do
      :ok =
        :meck.expect(:hackney, :post, fn
          "http://pleroma.local/api/v1/apps",
          [{"content-type", "application/x-www-form-urlencoded"}],
          "client_name=Kazarma&redirect_uris=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&scopes=read+write+follow+push&website=https%3A%2F%2Fgitlab.com%2Fkazarma%2Fkazarma",
          [with_body: true] ->
            {:ok, 200, [],
             ~S({"client_id":"CLIENT_ID","client_secret":"CLIENT_SECRET","id":"32","name":"Kazarma","redirect_uri":"urn:ietf:wg:oauth:2.0:oob","website":"https://gitlab.com/technostructures/kazarma/kazarma","vapid_key":"VAPID_KEY"})}
        end)

      assert {:needs_authorization,
              %OAuth2.Client{client_id: "CLIENT_ID", client_secret: "CLIENT_SECRET", site: @site},
              _uri} = client(@site, @ap_id)

      assert [%App{client_id: "CLIENT_ID", client_secret: "CLIENT_SECRET", site: @site}] =
               Repo.all(App)
    end

    test "doesn't request the server if an App has already been created" do
      Repo.insert!(%App{client_id: "CLIENT_ID", client_secret: "CLIENT_SECRET", site: @site})

      assert {:needs_authorization,
              %OAuth2.Client{client_id: "CLIENT_ID", client_secret: "CLIENT_SECRET", site: @site},
              _uri} = client(@site, @ap_id)
    end

    test "fetches an access token when given the authorization code" do
      :ok =
        :meck.expect(:hackney, :post, fn
          "http://pleroma.local/api/v1/apps",
          [{"content-type", "application/x-www-form-urlencoded"}],
          "client_name=Kazarma&redirect_uris=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&scopes=read+write+follow+push&website=https%3A%2F%2Fgitlab.com%2Fkazarma%2Fkazarma",
          [with_body: true] ->
            {:ok, 200, [],
             ~S({"client_id":"CLIENT_ID","client_secret":"CLIENT_SECRET","id":"32","name":"Kazarma","redirect_uri":"urn:ietf:wg:oauth:2.0:oob","website":"https://gitlab.com/technostructures/kazarma/kazarma","vapid_key":"VAPID_KEY"})}
        end)

      token_request_ref = make_ref()

      :ok =
        :meck.expect(:hackney, :request, fn
          :post,
          "http://pleroma.local/oauth/token",
          [
            {"accept", "application/x-www-form-urlencoded"},
            {"content-type", "application/x-www-form-urlencoded"},
            {"authorization", "Basic Q0xJRU5UX0lEOkNMSUVOVF9TRUNSRVQ="}
          ],
          "client_id=CLIENT_ID&code=CODE&grant_type=authorization_code&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob",
          [] ->
            {:ok, 200, [], token_request_ref}
        end)

      :ok =
        :meck.expect(:hackney, :body, fn
          ^token_request_ref ->
            {:ok,
             ~S({"access_token":"ACCESS_TOKEN","created_at":1640327584,"expires_in":600,"me":"http://pleroma.local/users/alice","refresh_token":"REFRESH_TOKEN","scope":"read write follow push","token_type":"Bearer"})}
        end)

      assert {:needs_authorization,
              %OAuth2.Client{client_id: "CLIENT_ID", client_secret: "CLIENT_SECRET", site: @site},
              _uri} = client(@site, @ap_id)

      set_code(@ap_id, "CODE")

      assert {:ok, %OAuth2.Client{token: %OAuth2.AccessToken{access_token: "ACCESS_TOKEN"}}} =
               client(@site, @ap_id)

      assert {:ok, %OAuth2.Client{token: %OAuth2.AccessToken{access_token: "ACCESS_TOKEN"}}} =
               client(@site, @ap_id)
    end
  end
end
