defmodule ActivityPubClient.Repo.Migrations.AddUsersTable do
  use Ecto.Migration

  def change do
    create table("ap_client_users") do
      add :ap_id, :string
      add :app_id, references(:ap_client_apps)
      add :code, :string
      add :token, :map

      timestamps()
    end

    create unique_index("ap_client_users", [:ap_id])
  end
end
