defmodule ActivityPubClient.Repo do
  @moduledoc false

  use Ecto.Repo,
    otp_app: :activity_pub_client,
    adapter: Ecto.Adapters.Postgres
end
